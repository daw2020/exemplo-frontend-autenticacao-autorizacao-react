import React from 'react';


import './App.css';


function App(){

  

 /* const [users, setUsers] = useState([]);
  const [name, setName] = useState('');
  const [password, setPassword] = useState('');
  const [login, setLogin] = useState('');
  const [email, setEmail] = useState('');
  const [errors, setErros] = useState({name: '', email: '', login: '', passord: ''})

  useEffect(() => {
    fetchUsers();
  }, [])

  const fetchUsers = async () => {
    try{
      console.log(`path:${process.env.REACT_APP_API_HOST}/user`)
      const response = await axios.get(`${process.env.REACT_APP_API_HOST}/user`);
      setUsers(response.data);
    }catch(error){
      console.error('Error fetching users: ', error);
    }
  }

const handleNameChange = (event) => {
  setName(event.target.value);
  setErros((prevErrors) => ({...prevErrors, name: ''}));
};

const handlePasswordChange = (event) => {
  setPassword(event.target.value);
  setErros((prevErrors) => ({...prevErrors, password: ''}));
};

const handleEmailChange = (event) => {
  setEmail(event.target.value);
  setErros((prevErrors) => ({...prevErrors, email: ''}));
}

const handleLoginChange = (event) => {
  setLogin(event.target.value);
  setErros((prevErrors) => ({...prevErrors, login: ''}));
}

const validateForm = () => {

  let isValid = true;
  const newErrors = {};

  if(name.trim() == ''){
    newErrors.name = "Campo nome é obrigatório";
    isValid = false;
  }

  if(email.trim() == ''){
    newErrors.email = "Campo e-mail é obrigatório";
    isValid = false;
  }

  if(login.trim() == ''){
    newErrors.login = "Campo login é obrigatório";
    isValid = false;
  }

  if(password.trim() == ''){
    newErrors.password = "Informe uma senha.";
    isValid = false;
  }

  setErros(newErrors);

  return isValid;

}

const addUser = async () => {

  if(!validateForm()){
    return;
  }

  try{

    const newUser = {name, email, login, password};
    await axios.post(`${process.env.REACT_APP_API_HOST}/user`, newUser );
    setName('');
    setPassword('');
    setEmail('');
    setLogin('');
    fetchUsers();
  }catch(error){
    console.error("Error adding user: ", error);
  }
};

  const deleteUser = async (userId) => {
    try{
      await axios.delete(`${process.env.REACT_APP_API_HOST}/user/${userId}`);
      fetchUsers();
    }catch(error){
      console.error('Error deleting user: ', error);
    }
  };

  return (
    <div>
      <Nav />
      <h1 className="title">Login</h1>
      <form className="form-container">
        <label>
          Nome:
          <input type="text" className="input-field" value={name} onChange={handleNameChange}/>
          {errors.name && <span className="error">{errors.name}</span>}
        </label>
        <br/>
        <label>
          E-mail:
          <input type="text" className="input-field" value={email} onChange={handleEmailChange}/>
          {errors.email && <span className="error">{errors.email}</span>}
        </label>
        <br/>
        <label>
          Login:
          <input type="text" className="input-field" value={login} onChange={handleLoginChange}/>
          {errors.login && <span className="error">{errors.login}</span>}
        </label>
        <br/>
        <label>
          Senha:
          <input type="password" className="input-field" value={password} onChange={handlePasswordChange}/>
          {errors.password && <span className="error">{errors.password}</span>}
        </label>
        <br />
        <button type="button" className="button" onClick={addUser}>Add User</button>
      </form>
      <h2>
        Usuários
      </h2>
      <ul>
        {users.map((user, index) => (
          <li key={index}>
            Nome: {user.name}, E-mail: {user.email}, Login: {user.login}, Senha: {user.password}
            <button type="button" onClick={() => deleteUser(user.id)}>Apagar</button>
          </li>
        ))}

      </ul>
    </div>
  )*/

}

export default App;